using System;

namespace LoopTasks
{
    public static class LoopTasks
    {
    
     /// <summary>
        /// Task 1
        /// </summary>
        public static int SumOfOddDigits(int n)
        {
          
          int result = 0;
                if (n >= 0)
                {
                    string number = n.ToString();
                    for (int i = 0; i < number.Length; i++)
                    {
                        int temp = (int)Char.GetNumericValue(number[i]);
                        if ((temp) % 2 != 0)
                        {
                            result = result + temp;
                        }
                    }
                }
            return result;
        }

        /// <summary>
        /// Task 2
        /// </summary>
        public static int NumberOfUnitsInBinaryRecord(int n)
        {
            string result = "";
                int sum = 0;
                if (n >= 0)
                {
                    while (n >= 1)
                    {
                        int remainder = n % 2;
                        result = Convert.ToString(remainder) + result;
                        sum += remainder;
                        n /= 2;
                    }

                    Console.WriteLine("Binary value = {0}", result);

                }
            
            return sum;
        }

        /// <summary>
        /// Task 3 
        /// </summary>
        public static int SumOfFirstNFibonacciNumbers(int n)
        {
            int sum = 0;
                string fibonahiLine = "";
                //ArrayList<>
                int[] fibonachi = new int [n];
                if (n >= 0)
                {
                    fibonachi[0] = 0;
                    for (int i = 1; i < n; i++)
                    {
                        if ((i == 1) || (i == 2))
                        { fibonachi[i] = 1; }
                        else
                        {
                            fibonachi[i] = fibonachi[i - 1] + fibonachi[i - 2];
                        }
                        fibonahiLine += fibonachi[i] + "  ";
                    }

                    for (int i = 0; i < n; i++)
                    {
                        sum += fibonachi[i];
                    }
                    Console.WriteLine("Fibonachi first {0} numbers {1}", n, fibonahiLine);
                    
                }
                return sum;
        }

    }
}
